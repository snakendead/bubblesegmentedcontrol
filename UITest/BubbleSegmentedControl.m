//
//  BubbleSegmentedControl.m
//  UITest
//
//  Created by developer2 on 22.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "BubbleSegmentedControl.h"
#import "BubbleView.h"
#import "AppDelegate.h"

@interface BubbleSegmentedControl()

@property (nonatomic) UIView *contentView;
@property (nonatomic) UIView *backLine;

@property (nonatomic) NSMutableArray <BubbleView *> *stateViews;

@property (nonatomic, strong) NSMutableArray <NSLayoutConstraint *> *addedConstraintsToRemove;

@property (nonatomic) NSMutableArray <NSString *> *titleStrings;
@property (nonatomic) NSMutableArray <NSString *> *selectedTitleStrings;

@end

@implementation BubbleSegmentedControl

// Initialization

- (instancetype)initWithFrame:(CGRect)frame statesCount:(NSUInteger)statesCount titlesArray:(NSArray <NSString *> *)titles selectedTitlesArray:(NSArray <NSString *> *)selectedTitles; {
    self = [super initWithFrame:frame];
    if (self) {
        self.statesCount = statesCount;
        [self baseInitWithDefaults:false];
        [self.titleStrings addObjectsFromArray:titles];
        [self.titleStrings addObjectsFromArray:selectedTitles];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInitWithDefaults:true];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self baseInitWithDefaults:false];
    }
    return self;
}

- (void)prepareForInterfaceBuilder {
    [self rootBaseInit];
}

- (void)rootBaseInit {
    self.clipsToBounds = NO;
    self.layer.masksToBounds = NO;
    self.titleStrings = [[NSMutableArray alloc] init];
    self.selectedTitleStrings = [[NSMutableArray alloc] init];
    self.stateViews = [[NSMutableArray alloc] init];
}

- (void)baseInitWithDefaults:(BOOL)withDefaults {
    [self rootBaseInit];
    if (withDefaults) { [self initDefaults]; }
    [self initViews];
    [self initGestures];
}

- (void)initDefaults {
    _height = 30;
    _statesCount = 3;
    _width = 40;
    _selectedWidth = 100;
    _autoSizeEnabled = false;
}

- (void)initViews {
    [self setupContentView];
    [self setupBackLine];
    [self reloadViews];
}

- (void)reloadViews {
    [self checkTitlesArrays];
    [self removeOldConstraints];
    [self reinstallViews];
}

- (void)reinstallViews {
    for (BubbleView *view in self.stateViews) {
        [view removeFromSuperview];
    }
    
    [self.stateViews removeAllObjects];
    self.backLine.backgroundColor = self.secondColor;
    [self setupStateViewsWithCount:self.statesCount];
}

// IBInspectable

- (void)setTintColor:(UIColor *)tintColor {
    _tintColor = tintColor;
}

- (void)setSecondColor:(UIColor *)secondColor {
    _secondColor = secondColor;
}

- (void)setStatesCount:(NSUInteger)statesCount {
    if (statesCount < 2) {
        _statesCount = 2;
    } else {
        _statesCount = statesCount;
    }
    [self reloadViews];
}

- (void)setCurrentStateIndex:(NSInteger)currentStateIndex {
    NSInteger old = _currentStateIndex;
    if (currentStateIndex < self.statesCount) {
        if (old == currentStateIndex) {
            _currentStateIndex = -1;
        } else {
            _currentStateIndex = currentStateIndex;
        }
    } else {
        _currentStateIndex = self.statesCount - 1;
    }
    if ([self.delegate respondsToSelector:@selector(segmentedControl:didChangeStateFromStateAtIndex:toStateAtIndex:)]) {
        [self.delegate segmentedControl:self didChangeStateFromStateAtIndex:old toStateAtIndex:_currentStateIndex];
    }

}

- (void)setWidth:(CGFloat)width {
    _width = width;
    [self reloadViews];
}

- (void)setHeight:(CGFloat)height {
    _height = height;
    [self reloadViews];
}

- (void)setSelectedWidth:(CGFloat)selectedWidth {
    _selectedWidth = selectedWidth;
    [self reloadViews];
}

- (void)setAutoSizeEnabled:(BOOL)autoSizeEnabled {
    _autoSizeEnabled = autoSizeEnabled;
    [self reloadViews];
}

// Views initialization

-(void)setupContentView {
    self.contentView = [[UIView alloc] initWithFrame:CGRectZero];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.contentView];
    [self setupContentViewConstraints];
}

-(void)initGestures {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewWasTapped:)];
    [self.contentView addGestureRecognizer:tap];
}

- (void)setupContentViewConstraints {
    NSLayoutConstraint *constraint;
    
    constraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.contentView.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView.superview attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.contentView.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView.superview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.contentView.superview addConstraint:constraint];
}

- (void)setupStateViewsWithCount:(NSInteger)count {
    for (NSInteger index = 0; index < count; index ++) {
        NSString *title = [self titleAtIndex:index];
        NSString *selectedTitle = [self selectedTitleAtIndex:index];
        BubbleView *bubbleView = [[BubbleView alloc] initWithTintColor:self.tintColor andSecondColor:self.secondColor andTitle:title andSelectedTitle:selectedTitle autoSizeOfContentWidth:_autoSizeEnabled];
        if (!_autoSizeEnabled) { [bubbleView setNormalStateWidth:_width andSelectedStateWidth:_selectedWidth]; }
        [bubbleView setSelected: self.currentStateIndex == index];
        [self.contentView addSubview:bubbleView];
        [bubbleView setupLabelConstraints];
        [self.stateViews addObject:bubbleView];
        if (0 == index) {
            [self setupFirstViewConstraints:bubbleView];
        } else {
            [self setupViewConstraints:bubbleView withPreviousView:[self.stateViews objectAtIndex:index - 1]];
        }
    }
    if (count > 2) { [self setupLastViewRightConstraint:[self.stateViews objectAtIndex:count - 1]]; }
}

- (void)setupFirstViewConstraints:(BubbleView *)view {
    NSLayoutConstraint *constraint;
    
    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view.superview attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:_height];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view.superview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:[view currentWidth]];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];
}

- (void)setupViewConstraints:(BubbleView *)view withPreviousView:(BubbleView *)previousView {
    NSLayoutConstraint *constraint;
    
    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeRight multiplier:1.0 constant:10];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:_height];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view.superview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:[view currentWidth]];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];
}

- (void)setupLastViewRightConstraint:(BubbleView *)view {
    NSLayoutConstraint *constraint;
    
    constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view.superview attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [view.superview addConstraint:constraint];
    
    BOOL widthConstraintIsInstalled = NO;
    for (NSLayoutConstraint *constraint in view.superview.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeWidth && (constraint.firstItem == view || constraint.secondItem == view)) {
            widthConstraintIsInstalled = YES;
        }
    }
    if (!widthConstraintIsInstalled) {
        constraint = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view.superview attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
        [self.addedConstraintsToRemove addObject:constraint];
        [view.superview addConstraint:constraint];
    }
}

- (void) setupBackLine {
    self.backLine = [[UIView alloc] initWithFrame:CGRectZero];
    self.backLine.translatesAutoresizingMaskIntoConstraints = NO;
    self.backLine.userInteractionEnabled = NO;
    [self.contentView addSubview:self.backLine];
    [self setupBackViewConstraints];
}

- (void)setupBackViewConstraints {
    NSLayoutConstraint *constraint;
    
    constraint = [NSLayoutConstraint constraintWithItem:self.backLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:2];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.backLine.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:self.backLine attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.backLine.superview attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.backLine.superview addConstraint:constraint];

    constraint = [NSLayoutConstraint constraintWithItem:self.backLine attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.backLine.superview attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.backLine.superview addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.backLine attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.backLine.superview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    [self.addedConstraintsToRemove addObject:constraint];
    [self.backLine.superview addConstraint:constraint];
}

// Public methods

- (void)setCurrentIndex:(NSInteger)stateIndex {
    [self setCurrentStateIndex:stateIndex];
    [self checkSelectedView];
}

- (void)fillWithTitlesArray:(NSArray <NSString *> *)titles selectedTitlesArray:(NSArray <NSString *> *)selectedTitles statesCount:(NSUInteger)statesCount {
    [self setStatesCount:statesCount];
    
    [self.titleStrings removeAllObjects];
    [self.selectedTitleStrings removeAllObjects];
    
    [self.titleStrings addObjectsFromArray:titles];
    [self.selectedTitleStrings addObjectsFromArray:selectedTitles];
    [self reloadViews];
}

- (void)addStateWithTitle:(NSString *)title andSelectedTitle:(NSString *)selectedTitle {
    _statesCount += 1;
    [_titleStrings addObject:title];
    [_selectedTitleStrings addObject:selectedTitle];
    [self reloadViews];
}

- (void)addStateWithTitle:(NSString *)title andSelectedTitle:(NSString *)selectedTitle atIndex:(NSUInteger)index {
    _statesCount += 1;
    [_titleStrings insertObject:title atIndex:index];
    [_selectedTitleStrings insertObject:selectedTitle atIndex:index];
    [self reloadViews];
}

- (void)removeStateAtIndex:(NSUInteger)index {
    if (_statesCount - 1 >= 2) {
        _statesCount -= 1;
        [_titleStrings removeObjectAtIndex:index];
        [_selectedTitleStrings removeObjectAtIndex:index];
        [self reloadViews];
    }
}


// Inner methods

- (void)checkSelectedView {
    [self reloadViews];
    [self unselectSelectedViewAtIndex:_currentStateIndex];
    if (_currentStateIndex >= 0) {
        [self setViewSelectedWithIndex:_currentStateIndex];
    }
}

- (void)unselectSelectedViewAtIndex:(NSInteger)index {
    BubbleView *view = [self viewAtIndex:index];
    [view setSelected:false];
}

- (void)setViewSelectedWithIndex:(NSInteger)index {
    BubbleView *view = [self viewAtIndex:index];
    [view setSelected:true];
}

- (BubbleView *)viewAtIndex:(NSUInteger)index {
    if (index >= self.stateViews.count) { return nil; }
    return [self.stateViews objectAtIndex:index];
}

- (NSString *)titleAtIndex:(NSUInteger)index {
    if (index >= self.titleStrings.count) { return @""; }
    return [self.titleStrings objectAtIndex:index];
}

- (NSString *)selectedTitleAtIndex:(NSUInteger)index {
    if (index >= self.selectedTitleStrings.count) { return @""; }
    return [self.selectedTitleStrings objectAtIndex:index];
}

- (void)removeOldConstraints {
    [self removeConstraintsFromSubTree:[NSSet setWithArray:self.addedConstraintsToRemove]];
    [self.addedConstraintsToRemove removeAllObjects];
}

- (void)checkTitlesArrays {
    self.titleStrings = [self clearArray:self.titleStrings];
    self.selectedTitleStrings = [self clearArray:self.selectedTitleStrings];
}

- (NSMutableArray *)clearArray:(NSMutableArray *)array {
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index < self.statesCount; index ++) {
        NSString *string = @"";
        if (index < array.count) {
            string = array[index];
        }
        [newArray addObject:string];
    }
    return newArray;
}


- (void)contentViewWasTapped:(UIGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.contentView];
    NSInteger state = [self stateFromTapPoint:point];
    [self setCurrentIndex:state];
}

- (NSInteger)stateFromTapPoint:(CGPoint)tapPoint {
    for (NSInteger index = 0; index < _stateViews.count; index ++) {
        BubbleView *bubbleView = _stateViews[index];
        CGRect rect = CGRectMake(bubbleView.frame.origin.x-10, bubbleView.frame.origin.y-10, bubbleView.frame.size.width + 10, bubbleView.frame.size.height + 10);
        if (CGRectContainsPoint(rect, tapPoint)) {
            return index;
            break;
        }
    }
    return 0;
}

@end

@implementation UIView (RemoveConstraints)

- (void)removeConstraintsFromSubTree:(NSSet <NSLayoutConstraint *> *)constraints {
    NSMutableArray <NSLayoutConstraint *> *constraintsToRemove = [[NSMutableArray alloc] init];
    for (NSLayoutConstraint *constraint in self.constraints) {
        if ([constraints containsObject:constraint]) {
            [constraintsToRemove addObject:constraint];
        }
    }
    [self removeConstraints:constraintsToRemove];
    for (UIView *view in self.subviews) {
        [view removeConstraintsFromSubTree:constraints];
    }
}

@end
