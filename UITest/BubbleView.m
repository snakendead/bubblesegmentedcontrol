//
//  BubbleView.m
//  UITest
//
//  Created by developer2 on 22.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "BubbleView.h"

@interface BubbleView()

@property (nonatomic) UILabel *label;

@property (nonatomic, assign) CGFloat normalWidth;
@property (nonatomic, assign) CGFloat selectedWidth;

@end

@implementation BubbleView

- (instancetype)initWithTintColor:(UIColor *)tintColor andSecondColor:(UIColor *)secondColor andTitle:(NSString *)title andSelectedTitle:(NSString *)selectedTitle autoSizeOfContentWidth:(BOOL)autoSizeOfContentWidth{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self baseInit];
        self.tintColor = tintColor;
        self.secondColor = secondColor;
        self.title = title;
        self.selectedTitle = selectedTitle;
        _autoSizeOfContentWidth = autoSizeOfContentWidth;
        [self reloadViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (void)baseInit {
    self.clipsToBounds = YES;
    self.layer.masksToBounds = NO;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.userInteractionEnabled = NO;
    [self initDefaults];
    [self initViews];
}

- (void)initDefaults {
    _isSelected = false;
    self.tintColor = [UIColor blackColor];
    self.secondColor = [UIColor whiteColor];
    self.title = @"Default";
    self.title = @"Default selected";
    self.normalWidth = 40.0;
    self.selectedWidth = 100.0;
    _autoSizeOfContentWidth = false;
}

- (void)initViews {
    self.label = [[UILabel alloc]initWithFrame:CGRectZero];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.backgroundColor = [UIColor clearColor];
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.label];
    [self reloadViews];
}

- (void)reloadViews {
    [self reloadState];
}

// Public

//- (void)setConstraintWidth:(CGFloat)width {
//    for (NSLayoutConstraint *constraint in self.superview.constraints) {
//        if (constraint.firstAttribute == NSLayoutAttributeWidth && (constraint.firstItem == self || constraint.secondItem == self)) {
//            if (constraint.secondItem == NSLayoutAttributeNotAnAttribute && (constraint.firstItem == nil || constraint.secondItem == nil)) {
//                constraint.constant = width;
//            }
//        }
//    }
//}

- (void)setSelected:(BOOL)state {
    _isSelected = state;
    [self reloadState];
}

- (void)setNormalStateWidth:(CGFloat)width andSelectedStateWidth:(CGFloat)selectedWidth {
    self.normalWidth = width;
    self.selectedWidth = selectedWidth;
}

- (CGFloat)currentWidth {
    CGFloat width = 0;
    if (_autoSizeOfContentWidth) {
        CGSize size = [self.label sizeThatFits:CGSizeMake(MAXFLOAT, self.label.frame.size.height)];
        width = size.width + 20; // with offsets
    } else {
        width = _isSelected ? self.selectedWidth : self.normalWidth;
    }
    return width;
}
// Inner methods

- (void)reloadState {
    self.label.textColor = _isSelected ? self.secondColor : self.tintColor;
    self.backgroundColor = _isSelected ? self.tintColor : self.secondColor;
    self.label.text = _isSelected ? self.selectedTitle : self.title;
}


- (void)setupLabelConstraints {
    NSLayoutConstraint *constraint;
    
    constraint = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
    [self.label.superview addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
    [self.label.superview addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    [self.label.superview addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    [self.label.superview addConstraint:constraint];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.width > self.frame.size.height ? self.frame.size.height/2 : self.frame.size.width/2;
}

@end
