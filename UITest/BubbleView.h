//
//  BubbleView.h
//  UITest
//
//  Created by developer2 on 22.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BubbleView : UIView

@property (nonatomic) UIColor *tintColor;
@property (nonatomic) UIColor *secondColor;

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *selectedTitle;

@property (nonatomic, assign, readonly) BOOL isSelected;
@property (nonatomic, assign, readonly) BOOL autoSizeOfContentWidth;

- (instancetype)initWithTintColor:(UIColor *)tintColor andSecondColor:(UIColor *)secondColor andTitle:(NSString *)title andSelectedTitle:(NSString *)selectedTitle autoSizeOfContentWidth:(BOOL)autoSizeOfContentWidth;

- (void)setupLabelConstraints;

- (void)setNormalStateWidth:(CGFloat)width andSelectedStateWidth:(CGFloat)selectedWidth;

- (void)setSelected:(BOOL)state;
- (CGFloat)currentWidth;
@end
