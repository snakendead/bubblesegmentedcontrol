//
//  ViewController.m
//  UITest
//
//  Created by developer2 on 03.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "ViewController.h"
#import "BubbleSegmentedControl.h"

@interface ViewController () <BubbleSegmentedControlDelegate>

@property (weak, nonatomic) IBOutlet BubbleSegmentedControl *segm;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_segm fillWithTitlesArray:@[@"fir", @"sec", @"ASDASDASDASD"] selectedTitlesArray:@[@"FIR SEL", @"SEC SEL", @"as"] statesCount:4];
    _segm.delegate = self;
    // Do any additional setup after loading the view, typically from a nib.
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentedControl:(BubbleSegmentedControl *)segmentedControl didChangeStateFromStateAtIndex:(NSInteger)fromIndex toStateAtIndex:(NSInteger)toIndex {
    NSLog(@"FROM - %d TO - %d", fromIndex, toIndex);
}

@end
