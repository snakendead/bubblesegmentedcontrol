//
//  BubbleSegmentedControl.h
//  UITest
//
//  Created by developer2 on 22.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface UIView (RemoveConstraints)

- (void)removeConstraintsFromSubTree:(NSSet <NSLayoutConstraint *> *)constraints;

@end

@class BubbleSegmentedControl;

@protocol BubbleSegmentedControlDelegate <NSObject>

- (void)segmentedControl:(BubbleSegmentedControl *)segmentedControl didChangeStateFromStateAtIndex:(NSInteger)fromIndex toStateAtIndex:(NSInteger)toIndex;

@end

@interface BubbleSegmentedControl : UIView

@property (nonatomic, weak) IBOutlet id <BubbleSegmentedControlDelegate> delegate;

@property (nonatomic) IBInspectable UIColor *tintColor;
@property (nonatomic) IBInspectable UIColor *secondColor;

@property (nonatomic, assign) IBInspectable CGFloat height;

@property (nonatomic, assign) IBInspectable BOOL autoSizeEnabled;
@property (nonatomic, assign) IBInspectable CGFloat width;
@property (nonatomic, assign) IBInspectable CGFloat selectedWidth;

@property (nonatomic, assign, readonly) IBInspectable NSUInteger statesCount;
@property (nonatomic, assign, readonly) IBInspectable NSInteger currentStateIndex; //not selected if < 0

@property (nonatomic, readonly) NSMutableArray <NSString *> *titleStrings;
@property (nonatomic, readonly) NSMutableArray <NSString *> *selectedTitleStrings;

- (instancetype)initWithFrame:(CGRect)frame statesCount:(NSUInteger)statesCount titlesArray:(NSArray <NSString *> *)titles selectedTitlesArray:(NSArray <NSString *> *)selectedTitles;

- (void)fillWithTitlesArray:(NSArray <NSString *> *)titles selectedTitlesArray:(NSArray <NSString *> *)selectedTitles statesCount:(NSUInteger)statesCount;

- (void)addStateWithTitle:(NSString *)title andSelectedTitle:(NSString *)selectedTitle;
- (void)addStateWithTitle:(NSString *)title andSelectedTitle:(NSString *)selectedTitle atIndex:(NSUInteger)index;
- (void)removeStateAtIndex:(NSUInteger)index;

- (void)setCurrentIndex:(NSInteger)stateIndex;
- (void)reloadViews;

@end